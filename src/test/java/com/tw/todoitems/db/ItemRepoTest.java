package com.tw.todoitems.db;

import com.tw.todoitems.model.Item;
import com.tw.todoitems.model.ItemStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

class ItemRepoTest {
    private static ItemRepo itemRepo;

    @BeforeAll
    static void init() {
        itemRepo = new ItemRepo();
    }

    @Test
    void should_return_true_when_add_student() {
        Item item = new Item("Learning Java");
        item.setStatus(ItemStatus.COMPLETED);
        Item output = itemRepo.addItem(item);
        Assertions.assertTrue(output.getId() > 0);
        Assertions.assertEquals(item.getText(), output.getText());
        Assertions.assertEquals(item.getStatus(), output.getStatus());
    }

    @Test
    void should_return_all_items_when_find_items() {
        Item item = new Item("Learning Java");
        item.setStatus(ItemStatus.COMPLETED);
        itemRepo.addItem(item);

        List<Item> items = itemRepo.findItems();
        Assertions.assertNotNull(items);
        Assertions.assertTrue(items.size() > 0);
    }

    @Test
    void should_return_true_when_update_item() {
        Item item = new Item("Learning Java");
        item.setStatus(ItemStatus.COMPLETED);
        itemRepo.addItem(item);

        List<Item> items = itemRepo.findItems();
        Optional<Item> itemOptional = items.stream().filter(item1 -> item1.getText().equals("Learning Java")).findFirst();
        Assertions.assertTrue(itemOptional.isPresent());
        itemOptional.map(e -> {
            e.setStatus(ItemStatus.ACTIVE);
            e.setText("Leaning JS");
            return itemRepo.updateItem(e);
        }).ifPresent(Assertions::assertTrue);
    }

    @Test
    void should_return_true_when_delete_item() {
        List<Item> items = itemRepo.findItems();
        items.forEach(item -> {
            boolean output = itemRepo.deleteItem(item.getId());
            Assertions.assertTrue(output);
        });
    }

}