package com.tw.todoitems.db;

import com.tw.todoitems.model.Item;
import com.tw.todoitems.model.ItemStatus;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ItemRepo {
    private Connector connector;

    public ItemRepo() {
        this.connector = new Connector();
    }

    public Item addItem(Item item) {
        String sql = "INSERT INTO ItemsList (text, status) VALUES (\""
                + item.getText()
                + "\", \""
                + item.getStatus().name()
                + "\");";
        String id = "SELECT id FROM ItemsList WHERE text = \"" + item.getText() +"\";";
        try (Connection conn = connector.createConnect(); Statement s = conn.createStatement();) {
            s.execute(sql);
            ResultSet rs = s.executeQuery(id);
            rs.next();
            item.setId(rs.getInt("id"));
            conn.close();
            return item;
        } catch (SQLException e) {
            System.out.println("Adding failed");
            System.out.println(e.toString());
        }
        return null;
    }

    public List<Item> findItems() {
        String sql = "SELECT * FROM ItemsList";
        List<Item> items = new ArrayList<>();

        try (PreparedStatement pstmt = connector.createConnect().prepareStatement(sql);
             ResultSet rs = pstmt.executeQuery(sql);) {
            while(rs.next()){
                Item item = new Item (rs.getInt("id"), rs.getString("text"), ItemStatus.valueOf(rs.getString("status")));
                items.add(item);
            }
        } catch (SQLException e) {
            System.out.println("Searching failed.");
            System.out.println(e.toString());
        }
        return items;
    }

    public boolean updateItem(Item item) {
        String sql = "UPDATE ItemsList SET text = ?, status = ? WHERE id = ?";

        try (PreparedStatement pstmt = connector.createConnect().prepareStatement(sql);) {

            pstmt.setString(1, item.getText());
            pstmt.setString(2, item.getStatus().toString());
            pstmt.setInt(3, item.getId());

            pstmt.execute();
            pstmt.close();
            return true;
        } catch (SQLException e) {
            System.out.println("Updating failed.");
            System.out.println(e.toString());
        }
        return false;
    }

    public boolean deleteItem(int id) {
        String sql = "DELETE FROM ItemsList WHERE id = " + id;

        try (PreparedStatement pstmt = connector.createConnect().prepareStatement(sql);) {
            pstmt.execute();
            pstmt.close();
            return true;
        } catch (SQLException e) {
            System.out.println("Deleting failed.");
            System.out.println(e.toString());
        }
        return false;
    }
}
